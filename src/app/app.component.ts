import { Component } from '@angular/core';
import { RoutingrecordsService } from './services/routingrecords/routingrecords.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private routingrecords: RoutingrecordsService) {
    this.routingrecords.clearHistory();
    this.routingrecords.loadRouting();
    console.log(this.routingrecords.getHistory());
  }
}
