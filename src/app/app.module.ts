import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';

import { HttpClientModule } from '@angular/common/http';
import { JuegosComponent } from './components/juegos/juegos.component';
import { TiendasComponent } from './components/tiendas/tiendas.component';
import { StocksComponent } from './components/stocks/stocks.component';
import { ClientesComponent } from './components/clientes/clientes.component';
import { CompaniasComponent } from './components/companias/companias.component'
import { AlertModule } from './modules/_alert';
import { FormjuegoComponent } from './forms/formjuego/formjuego.component';
import { FormcompaniaComponent } from './forms/formcompania/formcompania.component';
import { FormloginComponent } from './forms/formlogin/formlogin.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RoutingrecordsService } from './services/routingrecords/routingrecords.service';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    JuegosComponent,
    TiendasComponent,
    StocksComponent,
    ClientesComponent,
    CompaniasComponent,
    FormjuegoComponent,
    FormcompaniaComponent,
    FormloginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AlertModule,
    ReactiveFormsModule
  ],
  providers: [RoutingrecordsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
