import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { Cliente } from 'src/app/dto/Cliente/cliente';
import { catchError } from 'rxjs/operators';
import { HttpErrorResponse, HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertService } from 'src/app/modules/_alert';
import { environment } from 'src/environments/environment';
import { LoginService } from '../../loginservice/login.service';

@Injectable({
  providedIn: 'root'
})
export class ClientesService {
  url = environment.url;
  header = this.loginserv.getAuthHeaders();

  constructor(private http: HttpClient, private alert: AlertService, private loginserv: LoginService) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Error del Front
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Error desde el Back
      errorMessage = `||Codigo de Error: ${error.status}\n||Mensaje: ${error.message}`;
    }
    this.alert.error(errorMessage);
    return throwError(errorMessage);

  }

  getClientes(): Observable<Cliente[]> {
    console.log("Entro")
    return this.http.get<Cliente[]>(this.url + 'clientes').pipe(
      catchError(err => this.handleError(err))
    )
  }

  postCliente(body: Cliente): Observable<any> {
    return this.http.post<Cliente>(this.url + 'anadirCliente', body).pipe(
      catchError(err => this.handleError(err))
    )
  }

  putCliente(body: Cliente): Observable<any> {
    this.header.append("id", String(body.getId()));
    return this.http.put<Cliente>(this.url + 'editarCliente', body, { headers: this.header }).pipe(
      catchError(err => this.handleError(err))
    )
  }

  deleteliente(id: number): Observable<any> {
    this.header.append("id", String(id));
    return this.http.delete(this.url + 'borrarCliente', { headers: this.header }).pipe(
      catchError(err => this.handleError(err))
    )
  }
}
