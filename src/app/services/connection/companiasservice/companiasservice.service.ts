import { Injectable } from '@angular/core';
import { Compania } from 'src/app/dto/Compania/compania';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AlertService } from 'src/app/modules/_alert';
import { LoginService } from '../../loginservice/login.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class companiesService {
  url = environment.url;
  header = this.loginserv.getAuthHeaders();


  constructor(private http: HttpClient, private alert: AlertService, private loginserv: LoginService, private route: Router) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Error del Front
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Error desde el Back
      errorMessage = `||Codigo de Error: ${error.status}\n||Mensaje: ${error.message}`;
    }
    this.alert.error(errorMessage);
    if (error.status == 401) {
      this.route.navigate(["/login"])
    }
    return throwError(errorMessage);

  }

  getcompanies(): Observable<Compania[]> {
    this.header = this.loginserv.getAuthHeaders();
    return this.http.get<Compania[]>(this.url + 'companies').pipe(
      catchError(err => this.handleError(err))
    )
  }

  postCompania(body: Compania): Observable<any> {
    this.header = this.loginserv.getAuthHeaders();
    return this.http.post<Compania>(this.url + 'companies', body).pipe(
      catchError(err => this.handleError(err))
    )
  }

  putCompania(body: Compania): Observable<any> {
    this.header = this.loginserv.getAuthHeaders();
    return this.http.put<Compania>(this.url + 'companies/' + body.getId(), body, { headers: this.header }).pipe(
      catchError(err => this.handleError(err))
    )
  }

  deleteliente(id: number): Observable<any> {
    return this.http.delete(this.url + 'companies/' + id, { headers: this.header }).pipe(
      catchError(err => this.handleError(err))
    )
  }
}
