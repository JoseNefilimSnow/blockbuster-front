import { TestBed } from '@angular/core/testing';

import { TiendasserviceService } from './tiendasservice.service';

describe('TiendasserviceService', () => {
  let service: TiendasserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TiendasserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
