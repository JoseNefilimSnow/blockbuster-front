import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Juego } from 'src/app/dto/Juego/juego';
import { environment } from 'src/environments/environment'
import { Observable, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AlertService } from 'src/app/modules/_alert';
import { LoginService } from '../../loginservice/login.service';
@Injectable({
  providedIn: 'root'
})
export class JuegosService {
  url = environment.url;
  header = this.loginserv.getAuthHeaders();

  constructor(private http: HttpClient, private alert: AlertService, private loginserv: LoginService) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Error del Front
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Error desde el Back
      errorMessage = `||Codigo de Error: ${error.status}\n||Mensaje: ${error.message}`;
    }
    this.alert.error(errorMessage);
    return throwError(errorMessage);

  }

  getJuegos(): Observable<Juego[]> {
    this.header = this.loginserv.getAuthHeaders();
    console.log("Entro")
    return this.http.get<Juego[]>(this.url + 'juegos').pipe(
      catchError(err => this.handleError(err))
    )
  }

  postJuego(body: Juego): Observable<any> {
    this.header = this.loginserv.getAuthHeaders();
    return this.http.post<Juego>(this.url + 'juegos', body).pipe(
      catchError(err => this.handleError(err))
    )
  }

  putJuego(body: Juego): Observable<any> {
    this.header = this.loginserv.getAuthHeaders();
    return this.http.put<Juego>(this.url + 'juegos/' + body.getId(), body, { headers: this.header }).pipe(
      catchError(err => this.handleError(err))
    )
  }

  deleteJuego(id: number): Observable<any> {
    this.header = this.loginserv.getAuthHeaders();
    return this.http.delete(this.url + 'juegos/' + id, { headers: this.header }).pipe(
      catchError(err => this.handleError(err))
    )
  }
}
