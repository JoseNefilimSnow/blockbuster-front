import { Injectable } from '@angular/core';
import { filter } from 'rxjs/operators';
import { NavigationEnd, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
/**
 * Este servicio va a sernos util para enrutar más eficientemente, si me sale un error de "No estas autentificado",
 *  podemos acceder a la ruta anterior nada más loguearnos. Añado tambien otras funcionalidades que nos pueden ser 
 * utiles
 */
export class RoutingrecordsService {

  private history = [];

  constructor(
    private router: Router
  ) { }

  public loadRouting(): void {

    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(({ urlAfterRedirects }: NavigationEnd) => {
        this.history = [...this.history, urlAfterRedirects];
      });
  }

  public getHistory(): string[] {
    return this.history;
  }

  public getPreviousUrl(): string {
    return this.history[this.history.length - 2] || '/juegos';
  }
  public clearHistory() {
    this.history = [];
  }
}
