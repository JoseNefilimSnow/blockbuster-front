import { TestBed } from '@angular/core/testing';

import { RoutingrecordsService } from './routingrecords.service';

describe('RoutingrecordsService', () => {
  let service: RoutingrecordsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RoutingrecordsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
