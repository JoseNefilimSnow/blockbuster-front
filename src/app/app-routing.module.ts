import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JuegosComponent } from './components/juegos/juegos.component';
import { CompaniasComponent } from './components/companias/companias.component';
import { FormloginComponent } from './forms/formlogin/formlogin.component';
import { FormjuegoComponent } from './forms/formjuego/formjuego.component';


const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: FormloginComponent },
  { path: 'companias', component: CompaniasComponent },
  { path: 'juegos', component: JuegosComponent },
  { path: 'anadirJuego', component: FormjuegoComponent },
  { path: 'editarJuego', component: FormjuegoComponent },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
