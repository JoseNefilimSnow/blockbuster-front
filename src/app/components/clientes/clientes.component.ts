import { Component, OnInit } from '@angular/core';
import { Cliente } from 'src/app/dto/Cliente/cliente';
import { ClientesService } from 'src/app/services/connection/clientesservice/clientesservice.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {
  clientes: Cliente[] = [];

  constructor(private clienteserv: ClientesService) { }

  ngOnInit(): void {
    this.initializeClientes();
  }

  initializeClientes(): void {
    this.clientes = [];
    this.clienteserv.getClientes().subscribe(
      clientes => this.clientes = clientes
    )

  }

}
