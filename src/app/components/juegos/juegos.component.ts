import { Component, OnInit } from '@angular/core';
import { Juego } from 'src/app/dto/Juego/juego';
import { JuegosService } from 'src/app/services/connection/juegosservice/juegosservice.service';
import { RoutingrecordsService } from 'src/app/services/routingrecords/routingrecords.service';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-juegos',
  templateUrl: './juegos.component.html',
  styleUrls: ['./juegos.component.css']
})
export class JuegosComponent implements OnInit {
  juegos: Juego[] = [];

  constructor(private juegoserv: JuegosService, private routingrecords: RoutingrecordsService, private route: Router) {
    console.log(this.routingrecords.getHistory());
  }

  ngOnInit(): void {
    this.initializeJuegos();
  }

  initializeJuegos(): void {
    this.juegos = [];
    this.juegoserv.getJuegos().subscribe(
      juegos => this.juegos = juegos
    )

  }

  addJuego() {
    this.route.navigate(["/anadirJuego"])
  }
  editarJuego(indice) {
    let juego: NavigationExtras = {
      state: {
        juego: this.juegos[indice]
      }
    }
    this.route.navigate(["/editarJuego"], juego)
  }
  borrarJuego(indice) {
    this.juegoserv.deleteJuego(this.juegos[indice].idJuego)
  }

}
