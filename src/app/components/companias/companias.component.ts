import { Component, OnInit } from '@angular/core';
import { Compania } from 'src/app/dto/Compania/compania';
import { CompaniasService } from 'src/app/services/connection/companiasservice/companiasservice.service';

@Component({
  selector: 'app-companias',
  templateUrl: './companias.component.html',
  styleUrls: ['./companias.component.css']
})
export class CompaniasComponent implements OnInit {
  companias: Compania[] = [];

  constructor(private companiaserv: CompaniasService) { }

  ngOnInit(): void {
    this.initializeCompanias();
  }

  initializeCompanias(): void {
    this.companias = [];
    this.companiaserv.getCompanias().subscribe(
      companias => this.companias = companias
    )

  }

}
