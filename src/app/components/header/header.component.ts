import { Component, OnInit } from '@angular/core';
import { RoutingrecordsService } from 'src/app/services/routingrecords/routingrecords.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private routingrecords: RoutingrecordsService) {
  }

  ngOnInit(): void {
  }

}
