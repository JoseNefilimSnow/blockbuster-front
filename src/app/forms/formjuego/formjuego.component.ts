import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Compania } from 'src/app/dto/Compania/compania';
import { CompaniasService } from 'src/app/services/connection/companiasservice/companiasservice.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Juego } from 'src/app/dto/Juego/juego';
import { JuegosService } from 'src/app/services/connection/juegosservice/juegosservice.service';

@Component({
  selector: 'app-formjuego',
  templateUrl: './formjuego.component.html',
  styleUrls: ['./formjuego.component.css']
})
export class FormjuegoComponent implements OnInit {
  mode: string = "añadir"
  categorias = ["SHOOTER", "MOBA", "RPG", "MMORPG", "ROGUELIKE", "METROIDVANIA"]
  companiasList: Compania[];
  juegosForm: FormGroup;
  constructor(private formbuilder: FormBuilder, private juegoserv: JuegosService, private compservice: CompaniasService, private router: Router, private route: ActivatedRoute) {
    this.compservice.getCompanias().subscribe(companiasList => this.companiasList = companiasList)
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state.juego) {
        this.mode = "editar";
        let juego: Juego = this.router.getCurrentNavigation().extras.state.juego;
        this.juegosForm = this.formbuilder.group({
          titulo: [juego.titulo, [Validators.required]],
          fechaLanzamiento: [juego.fechaLanzamiento, [Validators.required]],
          categorias: [juego.categoria, [Validators.required]],
          companias: [juego.companias, [Validators.required]]
        })
      } else {
        this.mode = "añadir"
        this.juegosForm = this.formbuilder.group({
          titulo: ['', [Validators.required]],
          fechaLanzamiento: ['', [Validators.required]],
          categorias: ['', [Validators.required]],
          companias: ['', [Validators.required]]
        })
      }
    })

  }

  ngOnInit(): void {
  }
  enviarJuego() {
    switch (this.mode) {
      case "añadir":
        this.juegoserv.postJuego(this.juegosForm.value);
        break;
      case "editar":
        this.juegoserv.putJuego(this.juegosForm.value);
        break;
    }
    console.log(this.juegosForm.value)
  }

}
