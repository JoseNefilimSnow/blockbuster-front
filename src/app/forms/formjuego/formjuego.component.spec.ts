import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormjuegoComponent } from './formjuego.component';

describe('FormjuegoComponent', () => {
  let component: FormjuegoComponent;
  let fixture: ComponentFixture<FormjuegoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormjuegoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormjuegoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
