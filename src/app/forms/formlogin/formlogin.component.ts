import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { LoginService } from 'src/app/services/loginservice/login.service';
import { AlertService } from 'src/app/modules/_alert/alert.service';
import { RoutingrecordsService } from 'src/app/services/routingrecords/routingrecords.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-formlogin',
  templateUrl: './formlogin.component.html',
  styleUrls: ['./formlogin.component.css']
})
export class FormloginComponent implements OnInit {
  loginForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private loginserv: LoginService,
    private alert: AlertService, private routingrecords: RoutingrecordsService, private route: Router) {
    this.loginForm = this.formBuilder.group({
      user: ['', [Validators.required]],
      password: ['', [Validators.required]]
    })
  }

  ngOnInit(): void {
    this.loginForm.reset;
  }

  login() {
    this.loginserv.save(this.loginForm.value);
    this.alert.success("Ha iniciado sesión correctamente", { autoClose: true, keepAfterRouteChange: true, fade: true })
    this.route.navigate([this.routingrecords.getPreviousUrl()])
  }
}
