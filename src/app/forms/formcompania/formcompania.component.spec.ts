import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormcompaniaComponent } from './formcompania.component';

describe('FormcompaniaComponent', () => {
  let component: FormcompaniaComponent;
  let fixture: ComponentFixture<FormcompaniaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormcompaniaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormcompaniaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
