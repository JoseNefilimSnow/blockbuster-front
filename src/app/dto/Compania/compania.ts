import { Juego } from '../Juego/juego';

export class Compania {
    id: number;
    cif: string;
    nombre: string;
    // juegos:Juego[];

    constructor() { }

    getId() {
        return this.id;
    }

    setId(id: number) {
        this.id = id;
    }

    getCif(): string {
        return this.cif;
    }

    setCif(cif: string): void {
        this.cif = cif;
    }

    getNombre(): string {
        return this.nombre;
    }

    setNombre(nombre: string): void {
        this.nombre = nombre;
    }

    // getJuegos(): Juego[] {
    //     return this.juegos;
    // }

    // setJuegos(juegos:Juego[]):void{
    //     this.juegos=juegos;
    // }

}
