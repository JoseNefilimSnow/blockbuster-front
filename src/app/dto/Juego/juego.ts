import { Compania } from '../Compania/compania';

export class Juego {
    idJuego: number;
    titulo: string;
    fechaLanzamiento: Date;
    categoria: string;
    companias: Compania[]

    constructor() { }

    getId() {
        return this.idJuego;
    }

    setId(id: number) {
        this.idJuego = id;
    }

    getTitulo(): string {
        return this.titulo;
    }

    setTitulo(titulo: string): void {
        this.titulo = titulo;
    }

    getFechLanzamiento(): Date {
        return this.fechaLanzamiento;
    }

    getFechaLanzamiento(fechaLanzamiento: Date): void {
        this.fechaLanzamiento = fechaLanzamiento;
    }

    getCategoria(): string {
        return this.categoria;
    }

    setCategoria(categoria: string): void {
        this.categoria = categoria;
    }

}
