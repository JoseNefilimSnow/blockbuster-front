export class Cliente {
    id: number;
    nombre: string;
    dni: string;
    correo: string;
    fechaNacimiento: Date;
    //stocks:Stock[]

    constructor() { }
    getId(): number {
        return this.id;
    }

    setId(id: number): void {
        this.id = id;
    }

    getNombre(): string {
        return this.nombre;
    }

    setNombre(nombre): void {
        this.nombre = nombre;
    }

    getDni(): string {
        return this.dni;
    }

    setDni(dni: string): void {
        this.dni = dni
    }

    getCorreo(): string {
        return this.correo;
    }

    setCorreo(correo: string): void {
        this.correo = correo;
    }

    getFechaNac(): Date {
        return this.fechaNacimiento;
    }

    setFechaNac(fechaNac: Date): void {
        this.fechaNacimiento = fechaNac;
    }

    // getStocks():Stocks[]{
    //     return this.stocks;
    // }

    // setStocks(stocks:Stock[]):void{
    //     this.stocks=stocks;
    // }

}
